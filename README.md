# Collaborative Projects

Here are some examples of statistical analyses and reports that I have worked on in collaboration with physicians at Vanderbilt University Medical Center. Analyses are included as HTML files with code folding in the section titles, they are easiest to view if downloaded and then opened in your web browser. 
